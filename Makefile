
### Build Flags
#
CC = gcc
CFLAGS = -march=pentium4 -O2 -fno-guess-branch-probability -finline-functions -g -Wall -W
LDFLAGS = -lm

### predefined Variable
# override as needed via 'make VAR=new_value'
# !! no trailing slash
DESTDIR = ./instdir

### Default target
#
.PHONY:		all
all:		targets


### Subdirectories
#
dir := .
include $(dir)/Rules.mk


### Standard targets
#
.PHONY:		targets
targets:	$(TGT)

.PHONY:		clean
clean:
	rm -f $(CLEAN)
	rm -rf ./instdir

.PHONY:		install
install:	$(INST)

### special rules
#

#%.d: %.c
#	depend.sh 'dirname $*.c' $(CFLAGS) $*.c > $@

%.d: %.c
	gcc -MM -MG $*.c | sed -e 's@^\(.*\)\.o:@\1.d \1.o:@' > $@

