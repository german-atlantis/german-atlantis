/*
 * rand.h  PseudoRandomGenerator
 */


/* ask for random number */
extern unsigned int
rnd (void);

/* set start value for pseudo generator */
extern void
seed_rnd (unsigned int seed);

