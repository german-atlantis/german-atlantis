/*
 * rnd.c  Deterministic RandomNumberGenerator
 *
 * seen in some variations in
 * atlantis2 source 
 * and in
 * libc source from openbsd
 * Therefore this under BSD-License.
 *
 * License for this file: BSD
 */

static unsigned int rnd_no = 1;

unsigned int
rnd (void)
{
        rnd_no = rnd_no * 1103515245 + 12345;
        return ( (rnd_no >> 16) & 0x7FFF );
}

void
seed_rnd (unsigned int seed)
{
        rnd_no = seed;
}

