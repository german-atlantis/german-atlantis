
### header
#
sp := $(sp).x
dirstack_$(sp) := $(d)
d := $(dir)


### Local Stuff
#

OBJS_$(d) :=	\
	$(d)/atlantis.o $(d)/build.o \
	$(d)/combat.o \
	$(d)/constant.o \
	$(d)/contact.o \
	$(d)/creation.o \
	$(d)/economic.o \
	$(d)/give.o \
	$(d)/indicato.o \
	$(d)/language.o \
	$(d)/laws.o \
	$(d)/magic.o \
	$(d)/main.o \
	$(d)/monster.o \
	$(d)/movement.o \
	$(d)/reports.o \
	$(d)/rand.o \
	$(d)/save.o \
	$(d)/study.o \
	$(d)/terrain.o \
	$(d)/translate.o

DEPS_$(d) := $(OBJS_$(d):.o=.d)

TGT_$(d) := $(d)/atlantis $(d)/acheck

CLEAN := $(CLEAN) $(OBJS_$(d)) $(DEPS_$(d)) $(TGT_$(d))

TGT := $(TGT) $(TGT_$(d))

INST := $(INST) install_$(d)

# ----------------------------------------------------------- #

# -DDEBUG_MEMORY_USAGE :
#    In der Zusammenfassung wird aufgefuehrt, wieviele bytes ungefaehr gebraucht wurden.

#VERSION:=$(shell grep "^\#define RELEASE_VERSION" atlantis.h | awk '{print $$3}')

# ----------------------------------------------------------- #

$(d)/language.c $(d)/language.h: DIR_language := $(d)
$(d)/language.c $(d)/language.h: $(d)/language.def $(d)/mklang.pl
	cd $(DIR_language); \
	./mklang.pl;


$(d)/atlantis: $(OBJS_$(d)) 

$(d)/acheck: $(d)/acheck.c

install_$(d): $(d)/atlantis $(d)/acheck
	install -d $(DESTDIR)
	install -m u=rwx,go=r  $^ $(DESTDIR)/

### footer
#
-include $(DEPS_$(d))

d := $(dirstack_$(sp))
sp := $(basename $(sp))

