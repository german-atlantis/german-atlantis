#!/bin/bash
# (c) 2004 Thomas Gerigk
# license: GPL, LGPL, BSD-3-clause
#

tempname_i="temp.ga_z.$$.i";
tempname_o="temp.ga_z.$$.o";

cat > $tempname_i;
# i = mail; o = leer

#cat $tempname_i | formail -c -z -X "From:" -X "Message-ID:" | sort > $tempname_o;
echo "* # * # * # * # * # * # * # " > $tempname_o
echo >> $tempname_o;
./mime-mail.pl $tempname_i | fold -s -w 78 >> $tempname_o;
echo >> $tempname_o
# i = mail; o = artikel

cat $tempname_o >> zeitung_body

rm $tempname_i;
rm $tempname_o;

return 0

