
### header
#
sp := $(sp).x
dirstack_$(sp) := $(d)
d := $(dir)


### Local Stuff
#

SCRIPTS_$(d) := \
	$(d)/00_newbies \
	$(d)/1_pre_run.sh \
	$(d)/3_post_run.sh \
	$(d)/4_cleanup.sh \
	$(d)/5_ftp_upload \
	$(d)/ga_b.sh \
	$(d)/ga_k.sh \
	$(d)/ga_z.sh \
	$(d)/mimeconvert.pm \
	$(d)/mime-mail.pl \
	$(d)/mimesplitter.pm \
	$(d)/next_date.py \
	$(d)/send.sh \
	$(d)/subject.sh \
	$(d)/turn.sh \
	$(d)/work_here.sh


INST := $(INST) install_$(d)

# ----------------------------------------------------------- #

install_$(d): $(SCRIPTS_$(d))
	install -d $(DESTDIR)
	install -m u=rwx,go=r  $^ $(DESTDIR)/

### footer
#
-include $(DEPS_$(d))

d := $(dirstack_$(sp))
sp := $(basename $(sp))

