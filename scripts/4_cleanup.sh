#!/bin/bash
# laeuft in atlantis/

turn=`./turn.sh`

if test -n "$turn"
    then
    mv *.${turn} archive/
    
    cp data/${turn} archive/
    cd archive
    bzip2 ${turn}
    cd ..
    
    cd reports
    mkdir ${turn}
    mv *.nr *.cr ${turn}/
    rm *.zip
    cp kommentar zeitung mailit ${turn}/
    rm kommentar zeitung
    cd ..
else
    echo "Fehler: 4_cleanup.sh kann die aktuelle Runde nicht ermitteln"
fi
