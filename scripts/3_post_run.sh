#!/bin/bash
# laeuft in atlantis/

if test -n "$1"
    then
    nr=`./turn.sh`
    partikel=`./subject.sh`
    kennwort=$1
    
    cp -f Befehle Befehle.$nr
    echo > Befehle
    
    echo -n " " > Newbie
    
    echo > zeitung
    echo > kommentar
    echo "Kommentar ${partikel}" >> kommentar
    echo "Zeitung ${partikel}" >> zeitung
    
    cat zeitung_head >> zeitung
    cat zeitung_body >> zeitung
    echo > zeitung_body
    
    cat kommentar_head | sed -e "s/gz6L3i4h/${kennwort}/" >> kommentar
    
    cat parteien.$nr | sed -n -e '1,/Newbies/p' >> kommentar
    echo >> kommentar
    echo "---------------------------------------------" >> kommentar
    echo >> kommentar
    cat kommentar_body >> kommentar
    echo > kommentar_body
    
    cp -f zeitung zeitung.$nr
    cp -f kommentar kommentar.$nr
    
    cd reports
    /bin/bash mailit
    
else

    echo "Bitte Passwort fuer die Adressenliste im Kommentar angeben!!"

fi

