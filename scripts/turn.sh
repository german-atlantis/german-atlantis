#!/bin/bash
# (c) 2004 Thomas Gerigk
# license: GPL, LGPL, BSD-3-clause
#
# ./turn.sh
#
# gibt aktuelle Zugnummer des Spiels zurueck
#

echo `ls -1 data/ | grep "^[0-9][0-9]*$" | sort -n | tail -n 1`
