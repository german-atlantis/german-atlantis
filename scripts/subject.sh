#!/bin/bash
# (c) 2004 Thomas Gerigk
# license: GPL, LGPL, BSD-3-clause
#
# ./subject.sh
#
# gibt aktuelles Textpartikel fuer Betreff zurueck, zB:
# "im Dezember des Jahres 24"
#

TURN=`./turn.sh`

echo `head -n 1 parteien.$TURN | sed -e "s/Zusammenfassung fuer Atlantis //"`
