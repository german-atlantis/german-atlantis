#!/usr/bin/perl -w
#
# (c) 2003 Thomas Gerigk
#

# Nicht unterstuetzt:
#  content-disposition
#  ;format=flowed bei text/plain


@EXPORT=qw(convert);

use mimesplitter;


$tempbase='temp.mimeconvert.'.$$.'.';
$tempin=$tempbase."i";
$tempout=$tempbase."o";

# Wo ist das recode-Programm zu finden?
# Es konvertiert:
# -verschiedene Zeichensaetze
# -verschiedene Zeilenende-Kennungen
# unterstuetzt base64 und quoted-printable
$recode='/usr/local/bin/recode';
$recode_local_cs='iso-8859-15'; # Lokaler Zeichensatz

# gibt von Mail einen text/plain zurueck
# zur Not wird erst konvertiert

sub convert
{
    if ($#_ != 0 ) {die "\n!\'convert\' braucht ein Argument!\n";}
    my $msg=shift;
    
    my $charset;
    my $partsnr;
    my $i;
    my $retour="";
    
    my $s_res=splitter(0, 't', $msg);

    my $s_head=splitter(0, 'h', $msg);

    # Content-Transfer-Encoding: 7bit (default)
    # 8bit, binary, quoted-printable, base64
    my $cte;
    if ($s_head && ($s_head =~ m/^Content-Transfer-Encoding: (.*?)$/im  ))
    {
	$cte="/".$1;
	if ($cte =~ m/7bit/i or
	    $cte =~ m/8bit/i or
	    $cte =~ m/binary/i)
	{
	    $cte=""; # Loeschen da keine weitere Bearbeitung noetig
	}
    }
    else
    {
	$cte="";# Default
    }
    
    # Wieviele Teile?
    if ($s_res =~ m/^parts=(\d+)$/m)
    {
	$partsnr = $1 +0;
    }
    else
    {
	$partsnr=0;
    }

    # Welcher Zeichensatz?
    if($s_res =~ m/^charset=(.*?)$/im)
    {
	$charset=$1;
    }
    else
    {
	$charset="us-ascii";
    }

    # multi-alt -> den simpelsten konvertieren
    # ist immer der erste Teil
    if ($s_res =~ m/^multipart\/alternative$/im)
    {
	if ($partsnr < 1)
	{
	    die "\n!Angebliche multipart-mail ohne Mehr-Teile!\n";
	}
	    $retour=$retour.convert(splitter(1, 'm', $msg));
    }
    # multi-mixed -> alle Teile konvertieren
    # alle multi-(unbekannt) muessen als -mixed behandelt werden
    elsif ($s_res =~ m/^multipart\//im)
    {
	if ($partsnr < 1)
	{
	    die "\n!Angebliche multipart-mail ohne Mehr-Teile!\n";
	}
	for ($i=1; $i<=$partsnr; $i++)
	{
	    $retour=$retour.convert(splitter($i, 'm', $msg));
	}
    }
    # text/-Typen verarbeite
    # unbekannte text/-Typen als text/plain behandeln
    # text/plain; format=flowed wird erstmal nicht unterstuetzt (RFC2646)
    # message/rfc822 als text/ behandeln
    # sonstige message/* ignorieren
    elsif ($s_res =~ m/text\/html/im)
    {
#	open(TMPFILE, ">".$tempin)
	open (TMPFILE, "| recode -f $charset$cte..$recode_local_cs | w3m -T text/html -dump -cols 78 > $tempout")
	    or die "\n!mimeconvert:Kein Temp-File moeglich!\n";
	print TMPFILE splitter(0, 'b', $msg);
	close (TMPFILE);
#	`cat $tempin | recode $charset$cte..$recode_local_cs |w3m -T text/html -dump > $tempout`
#	    or die "\n!mimeconvert:Pipe kollabiert!\n";
	open(TMPFILE, "<".$tempout)
	    or die "\n!mimeconvert:Temp-File futsch!\n";
	undef $/;
	$retour=$retour.<TMPFILE>;
	close (TMPFILE);
#	`rm $tempin`;
	`rm $tempout`;
    }
    elsif ($s_res =~ m/^message\/rfc822$/im or
	   $s_res =~ m/^text\//im)
    {
#	open(TMPFILE, ">".$tempin)
	open (TMPFILE, "| recode -f $charset$cte..$recode_local_cs > $tempout")
	    or die "\n!mimeconvert:Kein Temp-File moeglich!\n";
	print TMPFILE splitter(0, 'b', $msg);
	close (TMPFILE);
#	`cat $tempin | recode $charset$cte..$recode_local_cs | cat > $tempout`
#	    or die "\n!mimeconvert:Pipe kollabiert!\n$charset\n$cte\n$recode_local_cs\n";
	open(TMPFILE, "<".$tempout)
	    or die "\n!mimeconvert:Temp-File futsch!\n";
	undef $/;
	$retour=$retour.<TMPFILE>;
	close (TMPFILE);
#	`rm $tempin`;
	`rm $tempout`;
    }
    # image/ ignorieren
    # audio/ ignorieren
    # video/ ignorieren

    # application/  !!!!pruefen

    # */octet-stream ignorieren

    return $retour;
}
