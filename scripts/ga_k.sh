#!/bin/bash
# (c) 2004 Thomas Gerigk
# license: GPL, LGPL, BSD-3-clause
#

tempname_i="temp.ga_k.$$.i";
tempname_o="temp.ga_k.$$.o";

cat > $tempname_i;
# i = Mail, o = leer

#cat $tempname_i | formail -c -z -X "Message-ID:" | sort > $tempname_o;
echo >> $tempname_o;
./mime-mail.pl $tempname_i | fold -s -w 78 >> $tempname_o;
echo >> $tempname.o;
# i = Mail; o = Kommentar

cat $tempname_o >> kommentar_body

rm $tempname_i;
rm $tempname_o;

return 0

