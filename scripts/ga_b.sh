#!/bin/bash
# (c) 2004 Thomas Gerigk
# license: GPL, LGPL, BSD-3-clause
#

tempname_i="temp.ga_b.$$.i";
tempname_o="temp.ga_b.$$.o";
tempname_a="temp.ga_b.$$.a";
tempname_r="temp.ga_b.$$.r";
logname="log.ga_b.$$";

# aus der Pipe zwischenspeichern
cat > $tempname_i;
# i = Mail

echo "LLL starting ga_b" > $logname

# From und Message-ID werden in Befehle uebernommen
# nur falls mal einer ne Partei kapern will
cat $tempname_i | formail -c -z -X "From:" -X "Message-ID:" | sort > $tempname_o;
echo >> $tempname_o;
# i = Mail; o = Header

echo "LLL From and Msg-ID:" >> $logname
cat $tempname_o >> $logname

# MIME filtern
./mime-mail.pl $tempname_i | sed -e's/BlankPARTEI/PARTEI/' >> $tempname_a;
# i = Mail; o = Header; a = de-MIME-d Befehle

# wird nicht mehr benoetigt
rm $tempname_i;
# i = leer; o = Header; a = Befehle

echo "LLL DeMIMEd content:" >> $logname
cat $tempname_a >> $logname

# nur GA-Anweisungen uebriglassen, unnoetiges blabla killen
cat $tempname_a | awk 'BEGIN {f=0;}  tolower($0) ~ /^[ \t]*partei/ {f=1;}  f==1 {print $0;}  tolower($0) ~ /^[ \t]*naechster/ {f=0;}  END {print "";}' > $tempname_i
# i = Befehle_pur; o = Header; a = Befehle

echo "LLL found commands:" >> $logname
cat $tempname_i >> $logname

rm $tempname_a
# i = Befehle_pur; o = Header; a = leer

echo "LLL acheck:" >> $logname
# checken
./acheck -q -s -w2 $tempname_i > $tempname_a;
if test $? -eq 0
then
    echo "ok" >> $logname
else
    echo "mist" >> $logname
fi
# i = Befehle_pur; o = Header; a = acheck

echo -e "Befehl erhalten von\n" > $tempname_r
cat $tempname_o >> $tempname_r
echo -e "\nErgebnis:\n" >> $tempname_r
cat $tempname_a >> $tempname_r
echo -e "\n" >> $tempname_r
# i = Befehle_pur; o = Header; a = acheck; r = reply

cat $tempname_o >> Befehle
cat $tempname_i >> Befehle

echo "LLL commands added to Befehle." >> $logname

# Befehle werden immer entgegengenommen
# Antwort geht an alle Partei-Adressen fuer die Befehle eingereicht wurden
# Die Adressen werden dem letzten mailit-script entnommen
# Status: i=Befehle a=acheckergebnis o=Sender r=frei
cat $tempname_i | awk 'tolower ($0) ~ /^[ \t]*partei[ \t]+[0-9]+/ {print $2;}' | \
while read i;
do
  echo "LLL recognized faction:" >> $logname;
  echo "$i" >> $logname;
  cat reports/mailit | grep -i "send[ \t]*$i " | awk '{print $3;}' | tr -d "\"" ;
done | \
while read a;
do
  echo "LLL found address:" >> $logname;
  echo "$a" >> $logname;
  mutt -s 'GA Check' -b devhost $a < $tempname_r 2>&1 1>> $logname;
  echo "LLL mail sent." >> $logname;
done


rm $tempname_r;
rm $tempname_a;
rm $tempname_i;
rm $tempname_o;

exit 0

