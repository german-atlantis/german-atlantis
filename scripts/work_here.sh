#!/bin/bash
# (c) 2004 Thomas Gerigk
# license: GPL, LGPL, BSD-3-clause
#
# ./anywhere/work_here script_to_call ...
#
# work_here wechselt in des Verzeichnis, wo es liegt,
# prueft ob script_to_call im gleichen Verzeichnis ist
# und ruft dort ebendieses auf mit den uebergebenen
# Parametern
#

function cwd_loc () {
    HIER=`echo ${1%/*}`
    if test -z "$HIER"
	then
	HIER="."
    fi
    if test -f "$HIER/$2"
	then
	echo "changing to $HIER"
	cd $HIER
	return 0
    else
	echo "script weiss nicht wo es ist"
	return 1
    fi;
}

if cwd_loc $0 $1
    then
    command=$1
    shift
    ./$command "$@"
fi
