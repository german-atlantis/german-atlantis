#!/usr/bin/perl -w
#
# (c) 2003 Thomas Gerigk <siato@towerlin.de>
# inspiriert von einem Skript von Tim Poepken
# (bzw. teilweise abgeschrieben)

use mimeconvert;

#print $#ARGV;
if ($#ARGV != 0) {die "!Brauche 1 Argumente!\n";}

#$wo=$ARGV[0];
#$was=$ARGV[1];
$wovon=shift;

#print $wo."\n".$was."\n".$wovon."\n";

open (MSGFILE, $wovon) || die "!Na, wo issen die Datei?!\n";
undef $/; # Mail-File am Stueck lesen.
$msg = <MSGFILE>;
close (MSGFILE);

print convert($msg);

