#!/usr/bin/perl -w
#
# (c) 2003 Thomas Gerigk <siato@towerlin.de>
# inspiriert von einem Skript von Tim Poepken
#


@EXPORT=qw(splitter);

# splitter(level, was, mail)
# mail: Die komplette Mail bzw. der jeweilige Teil bei multipart-Mails
# was: Soll der Header h, der Body b, Content-Type t oder
#      die Mail/der Teil m  zurueckgegeben werden?
# level: 0: 'was' vom aktueller mail liefern
#        1-x: selbstaufruf mit angegebenen Teil
#        x.y.0: 'was' vom y-ten Teil des x-ten Teil der Mail
#        a: 0-max. (also alle vorhandenen Teile)

sub splitter
{
    if ($#_ != 2) {die "\n\n!!\'splitter\' braucht 3 Argumente!!\n\n"; }

    my $wo=shift;
    my $was=shift;
    my $msg=shift;

    my $header;
    my $body;
    my $contype;
    my @conparam;
    my $conparamr;
    my $level;
    my $levelr;

    my @parts;
    my $begrenzer;

    my $elem; # nur ein lokaler zaehler

    my $retour="";

    ($header, $body)=split(/\n\n/,$msg,2); # trenne header und body
    $header =~ s/\n\s+/ /g; # multi-line zusammenziehen

    ($level, $levelr)=split(/\./,$wo,2); # level-parameter

    # content-type angegeben?
    if ( $header && $header =~ m/^content-type: (.*?)(?:\s*)\/(?:\s*)(.*?)(?:;(.*))?$/im )
    {
	$contype=$1."/".$2;
	$conparamr=$3;

#	print "Content-Type:\n".$contype."\n".$conparamr."\n";
	
	# alle content-type parameter rausziehen
	while ( $conparamr && ($conparamr =~ m/^(.*?)(?:;(.*))?$/ )  )
	{
	    push (@conparam, $1);
	    $conparamr=$2;
	}
	foreach $elem (@conparam)
	{
#	    print "1 ".$elem."\n";
	    if ($elem =~ m/^\s*(.*?)\s*=\s*(?:\")?(.*?)(?:\")?\s*$/ )
	    {
		$elem = $1."=".$2;
#		print "2 ".$elem."\n";
	    }
	}
	undef $elem;
    }
    else # Standard-Werte setzen gemaess RFC
    {
	$contype="text/plain";
	$conparam[0]="charset=us-ascii";
    };
    
    # Content-Type Parameter auf multipart untersuchen
    if ( $contype =~ m/^multipart\/.*$/i )
    {
	foreach $elem (@conparam)
	{
	    if ( $elem =~ m/^boundary\s*=\s*(?:\")?(.*?)(?:\")?$/i )
	    {
		$begrenzer=$1;
		last;
	    }
	}
	# body zerlegen
	# erster Teil vor begrenzer ist muell
	# letzter Teil nach begrenzer ebenso

#	print "3 ".$begrenzer."\n";

	if ( $begrenzer )
	{
	    @parts = split(/^--\Q$begrenzer/m, $body);

	    shift (@parts);
	    pop (@parts);
	    foreach $elem (@parts)
	    {
		$elem =~ s/^.*?\n//;
	    }
	}
    }

    # nu wirds knifflig
    # level:
    # 0  : nur das aktuelle ausspucken
    # 1- : in den betreffenden Teil (bei multipart) rekursieren
    # a  : alle obige


    if ($level eq "a" || $level==0 )
    {
	if ( $was eq "t" )
	{
	    $retour=$retour.$contype."\n";
	    foreach $elem (@conparam)
	    {
		$retour= $retour.$elem."\n";
	    }
	    if (@parts)
	    {
		$retour=$retour."parts=".($#parts+1)."\n";
	    }
	}
	elsif ($was eq "h")
	{
	    $retour=$retour.$header."\n";
	}
	elsif ($was eq "b")
	{
	    $retour=$retour.$body."";
	}
	elsif ($was eq "m")
	{
	    $retour=$retour.$msg."";
	}
    }

    if ($level eq "a" || $level > 0)
    {
	if ( ! $levelr)
	{
	    $levelr=0;
	}
    }

    if ($level eq "a")
    {
	foreach $elem (@parts)
	{
	    $retour=$retour."\n";
	    $retour=$retour.splitter($levelr, $was, $elem);
	}
    }
    elsif ($level > 0)
    {
	if ($parts[$level-1])
	{
	    $retour=$retour.splitter($levelr, $was, $parts[$level-1]);
	}
	else
	{
	    die "\n\n!!!Nicht vorhandener Teil angefordert!!!\n\n";
	}
    }
    
    return $retour;
    
}
