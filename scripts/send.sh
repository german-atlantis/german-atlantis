#!/bin/bash
# should be sourced by GA-generated mailit-script
# thus runs in ~/report/
# ~/report/*.nr *.cr
# ~/zeitung and kommentar should be linked to ~/reports/*
#
# requires $subject to be set to "im $month des Jahres $year"
#   (as returned by gamedate():reports.c

# links aufbauen von ./ nach ./reports/zeitung
# bzw. kommentar

export EMAIL=host@towerlin.de

if test -f zeitung
    then
    /bin/rm -f zeitung
fi
/bin/ln -f -s ../zeitung zeitung

if test -f kommentar
    then
    /bin/rm -f kommentar
fi
/bin/ln -f -s ../kommentar kommentar

# alte zip's loeschen
# (zip erneuert nur Dateien, statt neu anzulegen :-(
/bin/rm -f *.zip
/bin/rm -f *.tar.gz

# Funktion definieren:
#  Berichte und Texte verschicken

# Syntax:
# send <parteinummer> <email> <optionen>
# valid options:
#  ZIP   : alles als .zip senden
#  GZ    : alles als .tar.gz senden
#  CR    : ComputerReport schicken
#  AUS   : Auswertung schicken
#  ZINE  : Zeitung schicken
#  KOM   : Kommentar schicken
# ohne Kompression wird jedes als einzelne Mail verschickt.

send ()
{
    if test $# -lt 2
	then
	echo "send: zuwenig Parameter"
	return 0
    fi
    if test x${2}x = x@x
	then
	echo "send: keine eMail"
    fi
    local faction=$1
    local email=$2
    shift 2

    local list=""
    local arc=
    local arcp=
    local nr=
    local cr=
    local zeitung=
    local komment=
    local archive=
    local subj_add=""

    while test $# -gt 0
    do
      case $1 in
	  ZIP)
	      arc=".zip"
	      arcp="zip ARCHIV LIST"
	      ;;
	  GZ)
	      arc=".tar.gz"
	      arcp="tar -czf ARCHIV LIST"
	      ;;
	  CR)
	      cr="$faction.cr"
	      if test ! -f $cr
		  then
		  echo "send: angeforderter CR $cr nicht vorhanden"
		  cr=
	      else
		  list="$list $cr"
	      fi
	      ;;
	  AUS)
	      nr="$faction.nr"
	      if test ! -f $nr
		  then
		  echo "send: angeforderte Auswertung $nr nicht vorhanden"
		  nr=
	      else
		  list="$list $nr"
	      fi
	      ;;
	  ZINE)
	      zeitung="zeitung"
	      if test ! -f $zeitung
		  then
		  echo "send: angeforderte Zeitung nicht vorhanden"
		  zeitung=
	      else
		  list="$list $zeitung"
	      fi
	      ;;
	  KOM)
	      komment="kommentar"
	      if test ! -f $komment
		  then
		  echo "send: angeforderter Kommentar nicht vorhanden"
		  komment=
	      else
		  list="$list $komment"
	      fi
	      ;;
	  *)
	      echo "send: Unbekannte Option $1"
	      ;;
      esac
      shift
    done

    if test "${subject}" = ""
	then
	local subject="irgendwann"
    fi

    if test "${arc}" != ""
	then
	archive="$faction$arc"
	arcp=`echo $arcp | sed -e "s#ARCHIV#$archive#g" -e "s#LIST#$list#g"`
	eval $arcp
	mutt -s "German Atlantis $subject" -a $archive -- "$email" < ../message
    else
	if test "${nr}" != ""
	    then
	    mutt -s "German Atlantis $subject" "$email" < $nr
	fi
	if test "${cr}" != ""
	    then
	    mutt -s "German Atlantis CR $subject" "$email" < $cr
	fi
	if test "${zeitung}" != ""
	    then
	    mutt -s "German Atlantis Zeitung $subject" "$email" < $zeitung
	fi
	if test "${komment}" != ""
	    then
	    mutt -s "German Atlantis Kommentar $subject" "$email" < $komment
	fi
    fi	
    return
}
