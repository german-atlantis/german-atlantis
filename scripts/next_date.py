#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
Datum in 3 Wochen von heute oder Dateidatum berechnen.

Dies dient zur Berechnung des nächsten ZAT.
Dabei wird davon ausgegangen, dass heute ZAT ist,
oder die angegebene Datei am letzten ZAT erzeugt wurde.
"""

import datetime
import time
import sys
import os

if len(sys.argv) == 1:
    current = time.time()
elif len(sys.argv) == 2:
    filename = sys.argv[1]
    filestat = os.stat(filename)
    current = filestat.st_mtime
else:
    usage()
    sys.exit(1)

olddate = datetime.date.fromtimestamp(current)
# 3 Wochen Abstand
delta = datetime.timedelta(weeks=3)
newdate = olddate + delta

print newdate

