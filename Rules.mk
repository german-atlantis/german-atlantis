
### header
sp := $(sp).x
dirstack_$(sp) := $(d)
d := $(dir)


### Subdirectories
#
dir := src
include $(dir)/Rules.mk

dir := scripts
include $(dir)/Rules.mk

dir := templates
include $(dir)/Rules.mk

### footer
#-include $(DEPS_$(d))

d := $(dirstack_$(sp))
sp := $(basename $(sp))


