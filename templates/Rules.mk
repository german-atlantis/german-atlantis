
### header
#
sp := $(sp).x
dirstack_$(sp) := $(d)
d := $(dir)


### Local Stuff
#

TEXTS_$(d) := \
	$(d)/kommentar_head \
	$(d)/message \
	$(d)/newbie_message \
	$(d)/zeitung_head


INST := $(INST) install_$(d)

# ----------------------------------------------------------- #

install_$(d): $(TEXTS_$(d))
	install -d $(DESTDIR)
	install -m ugo=r  $^ $(DESTDIR)/

### footer
#
-include $(DEPS_$(d))

d := $(dirstack_$(sp))
sp := $(basename $(sp))

